package com.base.server.common.config;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @author HuTongFu
 * @Api：修饰整个类，描述Controller的作用
 * @ApiOperation：描述一个类的一个方法，或者说一个接口
 * @ApiParam：单个参数描述
 * @ApiModel：用对象来接收参数
 * @ApiProperty：用对象接收参数时，描述对象的一个字段
 * @ApiResponse：HTTP响应其中1个描述
 * @ApiResponses：HTTP响应整体描述
 * @ApiIgnore：使用该注解忽略这个API
 * @ApiError ：发生错误返回的信息
 * @ApiImplicitParam：一个请求参数
 * @ApiImplicitParams：多个请求参数
 * @description: swagger config
 * @since 2019/6/12 13:34
 */
@Configuration
public class Swagger2Config {

    private final SysCommonConfig sysCommonConfig;

    @Autowired
    public Swagger2Config(SysCommonConfig sysCommonConfig) {
        this.sysCommonConfig = sysCommonConfig;
    }

    @Bean
    public Docket fileApis() {
        ParameterBuilder requestParam = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        requestParam.name("Authorization").description("request Authorization")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).build(); //header中的Authorization参数非必填，传空也可以
        pars.add(requestParam.build()); //根据每个方法名也知道当前方法在设置什么参数
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("全部")//接口类别
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.regex("/api/rest.*"))
                .build()
                .apiInfo(commonApiInfo())
                .globalOperationParameters(pars)
                .enable(sysCommonConfig.isSwagger2Enable());
    }

    private ApiInfo commonApiInfo() {
        return new ApiInfoBuilder()
                .title("公共服务系统")
                .description("接口云平台-api文档")
//                .termsOfServiceUrl("http://www.qibaokeji.com")
                .version("1.0")
                .build();
    }
}
