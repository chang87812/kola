package com.base.server.modules.notice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import com.base.server.modules.notice.entity.Notice;
import com.base.server.modules.notice.entity.NoticeVo;
import com.base.server.modules.notice.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 通知表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2020-01-16
 */
@Api(tags = "通知表")
@RestController
@RequestMapping("api/rest/notice")
public class NoticeController {
    private final Logger logger = LoggerFactory.getLogger(NoticeController.class);

    @Autowired
    public NoticeService iNoticeService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getNoticeList(Notice notice) {
        try {
            QueryWrapper<Notice> queryWrapper = new QueryWrapper<>(notice);
            queryWrapper.eq("status", true);
            queryWrapper.orderByDesc("add_time");
            List<Notice> result = iNoticeService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getNoticeListPage(NoticeVo noticeVo,
                                            @RequestParam(defaultValue = "1") Long currentPage,
                                            @RequestParam(defaultValue = "10") Long pageSize) {
        try {
            Page<NoticeVo> page = new Page<>(currentPage, pageSize);
            IPage<NoticeVo> result = iNoticeService.selectByMyPage(page, noticeVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("查询单条数据")
    @GetMapping("/one")
    public CommonResponse getNoticeOne(NoticeVo noticeVo) {
        try {
            NoticeVo result = iNoticeService.selectNoticeOne(noticeVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("查询单条数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse noticeSave(@RequestBody NoticeVo noticeVo) {
        try {
            noticeVo.setId(UuidUtil.randomUUID());
            noticeVo = iNoticeService.save(noticeVo);
            return CommonResponse.ok(noticeVo);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse noticeUpdate(@RequestBody NoticeVo noticeVo) {
        try {
            noticeVo = iNoticeService.noticeUpdate(noticeVo);
            return CommonResponse.ok(noticeVo);
        } catch (Exception e) {
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse noticeDelete(@PathVariable String id) {
        try {
            int count = iNoticeService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids) {
        try {
            int count = iNoticeService.removeByIds(ids) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("批量删除对象异常", e);
        }
    }
}

