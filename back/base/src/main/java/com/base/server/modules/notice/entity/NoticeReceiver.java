package com.base.server.modules.notice.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息收件人表
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_base_notice_receiver")
@ApiModel(value="NoticeReceiver对象", description="消息收件人表")
public class NoticeReceiver extends Model<NoticeReceiver> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "通知主键id")
    private String noticeId;

    @ApiModelProperty(value = "接收人员id")
    private String userId;

    @ApiModelProperty(value = "是否已读(0否；1是)")
    private Boolean isRead;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
