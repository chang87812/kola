package com.base.server.modules.notice.mapper;

import com.base.server.modules.notice.entity.NoticeAttachment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 通知附件表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
public interface NoticeAttachmentMapper extends BaseMapper<NoticeAttachment> {

}
