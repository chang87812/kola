package com.base.server.modules.notice.service;

import com.base.server.modules.notice.entity.NoticeReceiver;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 消息收件人表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
public interface NoticeReceiverService extends IService<NoticeReceiver> {

}
