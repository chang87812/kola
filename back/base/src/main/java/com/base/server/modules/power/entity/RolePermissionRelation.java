package com.base.server.modules.power.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色与权限关联表
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_role_permission_relation")
@ApiModel(value="RolePermissionRelation对象", description="角色与权限关联表")
public class RolePermissionRelation extends Model<RolePermissionRelation> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "权限id")
    private String permissionId;

    @ApiModelProperty(value = "角色id")
    private String roleId;

    @ApiModelProperty(value = "权限类别")
    private String type;

}
