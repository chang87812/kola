package com.base.server.modules.power.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.base.server.modules.power.entity.MenuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface MenuInfoMapper extends BaseMapper<MenuInfo> {

    /**
     * 根据角色查询菜单
     *
     * @param roleIdArr
     * @return
     */
    List<MenuInfo> menuListByRole(@Param("roleIdArr") String[] roleIdArr);
}
