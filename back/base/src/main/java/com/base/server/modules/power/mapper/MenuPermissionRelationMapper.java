package com.base.server.modules.power.mapper;

import com.base.server.modules.power.entity.MenuPermissionRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单权限关联表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface MenuPermissionRelationMapper extends BaseMapper<MenuPermissionRelation> {

}
