package com.base.server.modules.power.service.impl;

import com.base.server.modules.power.entity.MenuPermissionRelation;
import com.base.server.modules.power.mapper.MenuPermissionRelationMapper;
import com.base.server.modules.power.service.MenuPermissionRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单权限关联表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class MenuPermissionRelationServiceImpl extends ServiceImpl<MenuPermissionRelationMapper, MenuPermissionRelation> implements MenuPermissionRelationService {

}
