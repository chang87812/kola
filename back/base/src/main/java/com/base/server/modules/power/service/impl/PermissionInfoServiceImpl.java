package com.base.server.modules.power.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.server.modules.power.entity.PermissionInfo;
import com.base.server.modules.power.entity.PermissionInfoVo;
import com.base.server.modules.power.mapper.PermissionInfoMapper;
import com.base.server.modules.power.service.PermissionInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class PermissionInfoServiceImpl extends ServiceImpl<PermissionInfoMapper, PermissionInfo> implements PermissionInfoService {

    @Resource
    private PermissionInfoMapper permissionInfoMapper;

    @Override
    public IPage<PermissionInfoVo> selectByMyPage(Page<PermissionInfoVo> page, PermissionInfoVo vo) {
        QueryWrapper<PermissionInfoVo> query = Wrappers.query(vo);
        if (StringUtils.isNoneBlank(vo.getName())) {
            query.like("tpi.name", vo.getName());
        }
        if (StringUtils.isNoneBlank(vo.getType())) {
            query.eq("tpi.type", vo.getType());
        }
        if (Objects.nonNull(vo.getStatus())) {
            query.eq("tpi.status", vo.getStatus());
        }
        query.orderByDesc("tpi.add_time");
        return permissionInfoMapper.selectByMyPage(page, query);
    }
}
