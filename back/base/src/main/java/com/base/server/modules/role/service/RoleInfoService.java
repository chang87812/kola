package com.base.server.modules.role.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.role.entity.RoleInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface RoleInfoService extends IService<RoleInfo> {

    IPage<RoleInfo> selectByPage(Page<RoleInfo> page, RoleInfo roleInfo);
}
