package com.base.server.modules.role.service.impl;

import com.base.server.modules.role.entity.RoleGroupRelation;
import com.base.server.modules.role.mapper.RoleGroupRelationMapper;
import com.base.server.modules.role.service.RoleGroupRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色与用户组关联表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class RoleGroupRelationServiceImpl extends ServiceImpl<RoleGroupRelationMapper, RoleGroupRelation> implements RoleGroupRelationService {

}
