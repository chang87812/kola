package com.base.server.modules.role.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.server.modules.role.entity.RoleInfo;
import com.base.server.modules.role.mapper.RoleInfoMapper;
import com.base.server.modules.role.service.RoleInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class RoleInfoServiceImpl extends ServiceImpl<RoleInfoMapper, RoleInfo> implements RoleInfoService {

    @Autowired
    private RoleInfoMapper roleInfoMapper;

    @Override
    public IPage<RoleInfo> selectByPage(Page<RoleInfo> page, RoleInfo roleInfo) {
        QueryWrapper<RoleInfo> queryWrapper = new QueryWrapper<>(roleInfo);
        queryWrapper.like("name", roleInfo.getName());
        return roleInfoMapper.selectByPage(page, queryWrapper);
    }
}
