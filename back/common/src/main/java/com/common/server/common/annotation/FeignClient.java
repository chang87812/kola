package com.common.server.common.annotation;

import java.lang.annotation.*;

/**
 * 使用原生feign 自定义feign client
 *
 * @author HuTongFu
 * @since 2019/6/30 15:33
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface FeignClient {
}
