package com.common.server.common.config.mybatisPlus;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.sql.*;

/**
 * 描述：mybatis 自定义 大字段类型处理
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/8/29 10:04 星期四
 */
public class MyBlobTypeHandler extends BaseTypeHandler<String> {

    private static final Log LOGGER = LogFactory.getLog(MyBlobTypeHandler.class);
    // 指定字符集
    private static final String DEFAULT_CHARSET = "utf-8";

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, String parameter, JdbcType jdbcType) throws SQLException {
        ByteArrayInputStream bis;
        try {
            // 把String转化成byte流
            bis = new ByteArrayInputStream(parameter.getBytes(DEFAULT_CHARSET));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Blob Encoding Error!");
        }
        preparedStatement.setBinaryStream(i, bis, parameter.length());
    }

    @Override
    public String getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
        {
            Blob blob = resultSet.getBlob(columnName);
            byte[] returnValue = null;
            String result = null;
            if (null != blob) {
                returnValue = blob.getBytes(1, (int) blob.length());
            }
            try {
                if (null != returnValue) {
                    // 把byte转化成string
                    result = new String(returnValue, DEFAULT_CHARSET);
                }
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Blob Encoding Error!");
            }
            return result;
        }
    }

    @Override
    public String getNullableResult(ResultSet resultSet, int columnName) throws SQLException {
        LOGGER.debug("enter function");
        String result = null;
        Blob blob = resultSet.getBlob(columnName);
        byte[] returnValue = null;
        if (null != blob) {
            returnValue = blob.getBytes(1, (int) blob.length());
        }
        try {
            // 把byte转化成string
            if (null != returnValue) {
                result = new String(returnValue, DEFAULT_CHARSET);
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Blob Encoding Error!");
        }
        LOGGER.debug("exit function");
        return result;
    }

    @Override
    public String getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
        Blob blob = callableStatement.getBlob(columnIndex);
        byte[] returnValue = null;
        String result = null;
        if (null != blob) {
            returnValue = blob.getBytes(1, (int) blob.length());
        }
        try {
            if (null != returnValue) {
                result = new String(returnValue, DEFAULT_CHARSET);
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Blob Encoding Error!");
        }
        return result;
    }
}
