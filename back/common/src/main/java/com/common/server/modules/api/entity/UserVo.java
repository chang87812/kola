package com.common.server.modules.api.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author HuTongFu
 * @description: 用于用户查询时接受页面传递的参数
 * @since 2019/6/10 9:55
 */
@Data
public class UserVo implements Serializable {

    private static final long serialVersionUID = 6410914128105686405L;
    //姓名
    private String name;

    //登录名称
    private String loginName;

    //登录类型（1门户；2后台管理）
    private String sysType;

    //登录密码
    private String password;

    //是否分配部门（1：是 0：否）
    private String isHasDepart;

    //是否分配角色 （1：是 0：否）
    private String isHasRole;
}
