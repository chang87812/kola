package com.common.server.modules.api.fallback;

import com.common.server.common.response.ResponseVo;
import com.common.server.modules.api.PermissionApi;
import com.common.server.modules.api.entity.MenuDto;
import com.common.server.modules.api.entity.UserDto;
import org.springframework.stereotype.Component;

/**
 * 描述: PermissionApiFallback
 * <p>
 * 作者: hu_to
 * 时间: 2020/04/13 14:19
 */
@Component
public class PermissionApiFallback implements PermissionApi {

    @Override
    public ResponseVo validToken(String token) {
        return ResponseVo.error("网络异常，请检查您的网络是否正常！");
    }

    @Override
    public ResponseVo logout() {
        return ResponseVo.error("网络异常，请检查您的网络是否正常！");
    }

    @Override
    public ResponseVo updPwd(UserDto userDto) {
        return ResponseVo.error("网络异常，请检查您的网络是否正常！");
    }

    @Override
    public ResponseVo getMenuTree(MenuDto menuDto) {
        return ResponseVo.error("网络异常，请检查您的网络是否正常！");
    }
}
