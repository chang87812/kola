package com.common.server.modules.dict.mapper;

import com.common.server.modules.dict.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-10-09
 */
public interface DictMapper extends BaseMapper<Dict> {

    List<Map<String, Object>> getKVList(@Param("type") String type);
}
