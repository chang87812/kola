package com.common.server.modules.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.server.modules.file.entity.FileEntity;

/**
 * <p>
 * 文件信息表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-10-08
 */
public interface FileMapper extends BaseMapper<FileEntity> {

}
