package com.common.server.modules.log.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统日志表
 * </p>
 *
 * @author ostrich
 * @since 2019-11-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_common_sys_log")
@ApiModel(value="SysLog对象", description="系统日志表")
public class SysLog extends Model<SysLog> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "系统类型")
    private String sysType;

    @ApiModelProperty(value = "操作类型")
    private String opType;

    @ApiModelProperty(value = "操作内容")
    private String opContent;

    @ApiModelProperty(value = "操作人")
    private String opUser;

    @ApiModelProperty(value = "操作时间")
    private LocalDateTime opTime;

    @ApiModelProperty(value = "操作ip")
    private String opIp;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
