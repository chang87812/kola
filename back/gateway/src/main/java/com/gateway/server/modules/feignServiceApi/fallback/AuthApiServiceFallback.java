package com.gateway.server.modules.feignServiceApi.fallback;

import com.gateway.server.common.response.CommonResponse;
import com.gateway.server.modules.feignServiceApi.AuthApiService;
import com.gateway.server.modules.feignServiceApi.entity.LoginVo;
import feign.Response;
import org.springframework.stereotype.Component;

/**
 * 描述: PermissionApiFallback
 * <p>
 * 作者: hu_to
 * 时间: 2020/04/13 14:19
 */
@Component
public class AuthApiServiceFallback implements AuthApiService {

    @Override
    public CommonResponse login(LoginVo vo) {
        return CommonResponse.error("网络异常，请检查您的网络是否正常！");
    }

    @Override
    public CommonResponse login(String account) {
        return CommonResponse.error("网络异常，请检查您的网络是否正常！");
    }

    @Override
    public CommonResponse validToken(String token) {
        return CommonResponse.error("网络异常，请检查您的网络是否正常！");
    }
}
