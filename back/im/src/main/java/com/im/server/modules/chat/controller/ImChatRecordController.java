package com.im.server.modules.chat.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.im.server.common.exception.SysCommonException;
import com.im.server.common.response.CommonResponse;
import com.im.server.common.utils.UuidUtil;
import com.im.server.modules.chat.entity.ImChatRecordVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

import com.im.server.modules.chat.service.ImChatRecordService;
import com.im.server.modules.chat.entity.ImChatRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 聊天记录 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-12-17
 */
@Api(tags = "聊天记录")
@RestController
@RequestMapping("api/rest/imChatRecord")
public class ImChatRecordController {
    private final Logger logger = LoggerFactory.getLogger(ImChatRecordController.class);

    @Autowired
    public ImChatRecordService iImChatRecordService;

    @ApiOperation("更新消息为已读")
    @PutMapping(value = "/updRead")
    public CommonResponse imChatRecordUpdate(ImChatRecordVo imChatRecordVo) {
        try {
            iImChatRecordService.imChatRecordUpdate(imChatRecordVo);
            return CommonResponse.ok();
        } catch (Exception e) {
            throw new SysCommonException("更新消息为已读异常", e);
        }
    }

    @ApiOperation("查询出未读消息记录")
    @GetMapping(value = "/unReadCount")
    public CommonResponse getUnReadImChatRecordCount(ImChatRecordVo imChatRecordVo) {
        try {
            Map<String, Object> result = iImChatRecordService.getUnReadImChatRecordCount(imChatRecordVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("查询出未读消息记录", e);
        }
    }

    @ApiOperation("无分页查询聊天列表数据")
    @GetMapping(value = "/list")
    public CommonResponse getImChatRecordList(ImChatRecordVo imChatRecordVo) {
        try {
            List<ImChatRecordVo> result = iImChatRecordService.selectRecordList(imChatRecordVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("无分页查询聊天记录详情数据")
    @GetMapping(value = "/info/list")
    public CommonResponse getImChatRecordInfoList(ImChatRecordVo imChatRecordVo) {
        try {
            List<ImChatRecordVo> result = iImChatRecordService.selectRecordInfoList(imChatRecordVo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getImChatRecordListPage(ImChatRecord imChatRecord) {
        try {
            Page<ImChatRecord> page = new Page<>();
            QueryWrapper<ImChatRecord> queryWrapper = new QueryWrapper<>(imChatRecord);
            IPage<ImChatRecord> result = iImChatRecordService.page(page, queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse imChatRecordSave(ImChatRecord imChatRecord) {
        try {
            imChatRecord.setId(UuidUtil.randomUUID());
            iImChatRecordService.save(imChatRecord);
            return CommonResponse.ok(imChatRecord);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("删除聊天记录")
    @DeleteMapping(value = "/del")
    public CommonResponse imChatRecordDelete(ImChatRecordVo imChatRecordVo) {
        try {
            int count = iImChatRecordService.imChatRecordDelete(imChatRecordVo) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids) {
        try {
            int count = iImChatRecordService.removeByIds(ids) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("批量删除对象异常", e);
        }
    }
}

