package com.im.server.modules.chat.mapper;

import com.im.server.modules.chat.entity.ImChatRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.im.server.modules.chat.entity.ImChatRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 聊天记录 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-12-17
 */
public interface ImChatRecordMapper extends BaseMapper<ImChatRecord> {

    /**
     * 查询聊天列表
     *
     * @param imChatRecordVo
     * @return
     */
    List<ImChatRecordVo> selectRecordList(@Param("vo") ImChatRecordVo imChatRecordVo);

    /**
     * 保存聊天记录和用户关系
     *
     * @param imChatRecordVo
     * @return
     */
    boolean saveUserImRelation(@Param("vo") ImChatRecordVo imChatRecordVo);

    /**
     * 查询消息记录
     *
     * @param imChatRecordVo
     * @return
     */
    List<ImChatRecordVo> selectRecordInfoList(@Param("vo") ImChatRecordVo imChatRecordVo);

    /**
     * 查询用户消息记录数量
     *
     * @param imChatRecordVo
     * @return
     */
    Map<String, Object> getUnReadImChatRecordCount(@Param("vo") ImChatRecordVo imChatRecordVo);

    /**
     * 更新聊天记录
     *
     * @param imChatRecordVo
     * @return
     */
    int updateRecord(@Param("vo") ImChatRecordVo imChatRecordVo);
}
