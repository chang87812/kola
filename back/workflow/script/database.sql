-- 创建流程引擎系统权限视图
CREATE VIEW base_server.act_id_user AS SELECT
	`u`.`id` AS `ID_`,
	0 AS `REV_`,
	`u`.`login_name` AS `FIRST_`,
	`u`.`name` AS `LAST_`,
	`u`.`email` AS `EMAIL_`,
	`u`.`password` AS `PWD_`,
	`u`.`header` AS `PICTURE_ID_`
FROM
	`base_server`.`t_user_info` `u`
WHERE
	u. STATUS = 1;

-- ----------------------------------------------------------- -------------------------------------------------------------
CREATE VIEW base_server.act_id_group AS SELECT
	`r`.`id` AS `ID_`,
	0 AS `REV_`,
	`r`.`name` AS `NAME_`,
	'role' AS `TYPE_`,
	0 AS `PID_`
FROM
	`base_server`.`t_role_info` `r`
WHERE
	r. STATUS = 1;

-- ----------------------------------------------------------- -------------------------------------------------------------
CREATE VIEW base_server.act_id_membership AS SELECT
	`ur`.`user_id` AS `USER_ID_`,
	ur.role_id AS `GROUP_ID_`
FROM
	`base_server`.`t_user_role_relation` `ur`