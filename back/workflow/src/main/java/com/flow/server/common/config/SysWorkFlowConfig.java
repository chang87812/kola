package com.flow.server.common.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.activiti.engine.*;
import org.activiti.engine.impl.history.HistoryLevel;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * 描述：流程引擎配置类
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/16 9:50
 */
@Configuration
public class SysWorkFlowConfig implements ProcessEngineConfigurationConfigurer {

    @Resource
    private DataSource dataSource;

    @Resource
    private PlatformTransactionManager platformTransactionManager;

//    @Resource
//    private CustomProcessDiagramGeneratorI customProcessDiagramGeneratorI;

    @Override
    public void configure(SpringProcessEngineConfiguration configuration) {
        // 流程图字体
        configuration.setActivityFontName("宋体");
        configuration.setLabelFontName("宋体");
        configuration.setAnnotationFontName("宋体");

        configuration.setDatabaseType("mysql");
        configuration.setDataSource(dataSource);
        configuration.setTransactionManager(platformTransactionManager);
        configuration.setDatabaseSchemaUpdate("true");
        configuration.setIdGenerator(uuidGenerator());
        //控制是否使用activiti流程自带的权限管理，如果设置为false需要自己定义一个权限管理，本系统使用视图方式
        configuration.setDbIdentityUsed(false);

        configuration.setHistoryLevel(HistoryLevel.FULL);
        //自定义流程图样式（如果使用自定义流程图样式，需要在部署的时候人工生成图片部署到数据库中）
//        configuration.setProcessDiagramGenerator(customProcessDiagramGeneratorI);
    }

    @Bean
    public ProcessEngine processEngine(SpringProcessEngineConfiguration configuration) {
        return configuration.buildProcessEngine();
    }

    @Bean("uuidGenerator")
    public UUIDGenerator uuidGenerator() {
        return new UUIDGenerator();
    }

    @Bean
    public RepositoryService repositoryService(ProcessEngine processEngine) {
        return processEngine.getRepositoryService();
    }

    @Bean
    public RuntimeService runtimeService(ProcessEngine processEngine) {
        return processEngine.getRuntimeService();
    }

    @Bean
    public TaskService taskService(ProcessEngine processEngine) {
        return processEngine.getTaskService();
    }

    @Bean
    public HistoryService historyService(ProcessEngine processEngine) {
        return processEngine.getHistoryService();
    }

    @Bean
    public FormService formService(ProcessEngine processEngine) {
        return processEngine.getFormService();
    }

    @Bean
    public ManagementService managementService(ProcessEngine processEngine) {
        return processEngine.getManagementService();
    }

    @Bean
    public IdentityService identityService(ProcessEngine processEngine) {
        return processEngine.getIdentityService();
    }

    @Bean
    public DynamicBpmnService dynamicBpmnService(ProcessEngine processEngine) {
        return processEngine.getDynamicBpmnService();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

}
