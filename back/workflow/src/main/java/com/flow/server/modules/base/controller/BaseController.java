package com.flow.server.modules.base.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.activiti.engine.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * 描述：基础controller
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/17 16:11 星期五
 */
public class BaseController {

    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    public RepositoryService repositoryService;
    @Resource
    public ObjectMapper objectMapper;

}
