package com.flow.server.modules.base.service;

/**
 * 描述：基础service
 * <p>
 * 作者：hutongfu
 * 时间：2020/1/18 11:27 星期六
 */
public interface IBaseService<T> {
    /**
     * 根据模型id删除信息
     *
     * @param modelId
     */
    public void deleteById(String modelId);

    public void deleteByObject(T t);
}
