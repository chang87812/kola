package com.flow.server.modules.process.cmd;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Map;

/**
 * 描述：流程实例 cmd
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/9/5 13:45 星期四
 */
@Getter
@Setter
@Builder
@Accessors(chain = true)
public class ProcessInstanceCmd implements Serializable {
    private static final long serialVersionUID = -6645560115926973685L;

    //流程实例id
    private String instanceId;

    //流程实例名称
    private String instanceName;

    //流程定义key
    private String processDefKey;

    //流程分类
    private String category;

    //流程定义id
    private String processDefId;

    //任务定义key
    private String taskDefKey;

    //流程业务id
    private String businessId;

    //扩展流程实例id
    private String extInstanceId;

    //扩展流程实例名称
    private String extInstanceName;

    //流程发起人id
    private String startUserId;

    //流程发起人
    private String startUserName;

    //是否草稿
    private String isDraft;

    //流程变量
    private Map<String, Object> variables;

    //页码
    private int pageNum;

    //每页大小
    private int pageSize;

    @Tolerate
    public ProcessInstanceCmd() {
    }
}
