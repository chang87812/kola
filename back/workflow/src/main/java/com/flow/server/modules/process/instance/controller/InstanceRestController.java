package com.flow.server.modules.process.instance.controller;

import com.flow.server.common.response.CommonResponse;
import com.flow.server.modules.base.controller.BaseController;
import com.flow.server.modules.process.instance.service.IInstanceRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 描述：流程实例前端控制器
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/19 9:12 星期日
 */
@Api("流程实例")
@RestController
@RequestMapping("api/rest/instance")
public class InstanceRestController extends BaseController {

    @Autowired
    private IInstanceRestService iInstanceRestService;

    @ApiOperation("发起流程实例")
    @PostMapping("/start")
    public CommonResponse startProcessInstance(HttpServletRequest request) {
        boolean flag = iInstanceRestService.startProcessInstance(request);
        return flag ? CommonResponse.ok() : CommonResponse.error();
    }

    @ApiOperation("分页查询运行中的流程实例")
    @GetMapping("/list/page")
    public CommonResponse getListPage(HttpServletRequest request) {
        Map<String, Object> result = iInstanceRestService.getListPage(request);
        return CommonResponse.ok(result);
    }

    @ApiOperation("根据流程实例id获取运行时流程图")
    @GetMapping("/img/{instanceId}")
    public void getWorkflowImage(HttpServletResponse response, @PathVariable String instanceId) {
        iInstanceRestService.getRunWorkflowImage(instanceId, response);
    }
}
