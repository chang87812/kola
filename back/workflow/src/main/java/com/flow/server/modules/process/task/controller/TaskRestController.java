package com.flow.server.modules.process.task.controller;

import com.flow.server.common.response.CommonResponse;
import com.flow.server.modules.base.controller.BaseController;
import com.flow.server.modules.process.task.service.ITaskRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 描述：任务办理 前端控制器
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/20 9:36 星期一
 */
@Api(tags = "流程实例流转处理")
@RestController
@RequestMapping("api/rest/task")
public class TaskRestController extends BaseController {

    @Autowired
    private ITaskRestService iTaskRestService;

    @ApiOperation("(解除)锁定流程实例")
    @PostMapping("/{type}/{taskId}/{uId}")
    public CommonResponse ucTask(@PathVariable String uId, @PathVariable String taskId, @PathVariable String type) {
        boolean flag = iTaskRestService.ucTask(uId, taskId, type);
        return flag ? CommonResponse.ok() : CommonResponse.error();
    }

    @ApiOperation("办理流程任务")
    @PostMapping("/do/{taskId}/{uId}")
    public CommonResponse doTask(@PathVariable String uId, @PathVariable String taskId, Map<String, Object> var) {
        boolean flag = iTaskRestService.doTask(uId, taskId, var);
        return flag ? CommonResponse.ok() : CommonResponse.error();
    }

    @ApiOperation("转办流程任务")
    @PostMapping("/turn/{taskId}/{uId}")
    public CommonResponse turnTask(@PathVariable String uId, @PathVariable String taskId) {
        boolean flag = iTaskRestService.turnTask(uId, taskId);
        return flag ? CommonResponse.ok() : CommonResponse.error();
    }

    @ApiOperation("挂起/激活流程任务")
    @PostMapping("/{type}/{instanceId}")
    public CommonResponse asTask(@PathVariable String instanceId, @PathVariable String type) {
        boolean flag = iTaskRestService.asTask(instanceId, type);
        return flag ? CommonResponse.ok() : CommonResponse.error();
    }
}
